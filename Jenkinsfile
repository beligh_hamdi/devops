
node {
    stage 'Checkout' 
        git url:'https://github.com/alecharp/simple-app.git' , branch: 'develop'

        sh 'git rev-parse HEAD > GIT_COMMIT'
        short_commit = readFile('GIT_COMMIT').trim().take(7)
        sh 'rm GIT_COMMIT'
   
    stage 'Build' 
        withMaven { sh 'mvn clean verify' }
   
        step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/*.xml'])
        step([$class: 'JUnitResultArchiver', testResults: '**/target/failsafe-reports/*.xml'])

        dir('target') { archive: '*.jar'}

        stash name: 'binary', includes: 'target/*.jar'
        dir('src/main/docker') { stash name: 'docker', includes: 'Dockerfile' }
}

def withMaven(def body) {
    def java = tool 'JDK8'
    def maven = tool 'M3'
       
    withEnv(["JAVA_HOME=${java}","PATH+MAVEN=${maven}/bin"]) {
        body.call()
    }
} 

node('docker') {
    unstash 'docker'
    unstash 'binary'

    stage 'Build Docker Image' 
        image = docker.build("alecharp/simple-app:${short_commit}")
        container = image.run('-P')
        sh "docker port ${container.id} 8080 > DOCKER_IP"
        ip = readFile('DOCKER_IP').trim()
        sh 'rm DOCKER_IP'
}

stage 'Validation' 
    try {
    input message: "http://${ip}. Is it Ok?, ok: 'Publish it'"
    } finally {
        node('docker') {
        container.stop() 
        }
    }

    node('docker') {
        stage 'Publishing' 
            docker.withRegistry('http://localhost:5000') {
                image.push('latest');
            }
    }
 